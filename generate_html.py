from common_imports import *

from xml.dom import minidom
from xml.etree.ElementTree import Element as E, SubElement as SE, tostring

import sqlite3

AYA_WORDS = OD()

HARAKAT = ["ي", "و", "ه", "ش", "س", "ق", "ف", "غ", "ع",
           "ض", "ص", "ن", "م", "ل", "ك", "ظ", "ط", "ز", "ر", "ذ", "د",
           "خ", "ح", "ج", "ث", "ت", "ب", "ا", "ة"]

HARAKAT.extend(["إ", "آ", "أ", "ئ", "ء", "ؤ", "ى"])

def fix_text(text):
    res = text.replace("آ", "ا")
    res = res.replace("ء", "")
    res = res.replace("ئ", "")
    res = res.replace("أ", "ا")
    res = res.replace("ي", "ى")
    res = res.replace("لا ىستحىى ان", "لا ىستحى ان")
    res = res.replace("فاداراتم", "فادارتم")
    return res

quran_file = "quran-simple-clean.txt"
# quran_file = "quran-uthmani-min.txt"

with open(quran_file, encoding="utf-8") as f:
    for line in f.read().split("\n"):
        if not line or line.startswith("#"):
            continue
        sura, aya, text = line.split("|")
        AYA_WORDS["%s, %s"%(sura, aya)] = fix_text(text)

def get_sura_aya_text(sura, aya):
    db = sqlite3.connect("hafs_qurancomplex_fixed.sqlite")
    text = db.execute("select text_text from hafs_qurancomplex_fixed where "
                      "sura=%s and text_number=%s"%(sura, aya)).fetchone()[0]
    # Browser (firefox, chrome) fixes
    # this fixed the rendering of waliyyiya Allah (e.g. 7:196) on Chrome, I don't know a fix for Firefox however
    text = text.replace("ـِّۧ", "ـۧ‏ِّ‏")
    return text

def get_sura_aya_rows(sura, aya):
    db = sqlite3.connect("hafs_qurancomplex_pages_fixed.sqlite")
    _lines = db.execute("select text from hafs_qurancomplex_pages_fixed "
                      "where "
                      "sura=%s and ayah=%s"%(sura, aya)).fetchall()
    return [line[0] for line in _lines]

def leave_harakat_only(text):
    res = ""
    for c in text:
        if c in HARAKAT + [" "]:
            res += c
    return fix_text(res)

def generate_html():
    with open(sys.argv[1], encoding="utf-8") as f:
        data = json_load(f, object_pairs_hook=OD)
    for sura_aya in data["ayat"]:
        print(sura_aya)
        sura, aya = sura_aya.split(", ")
        # if not (18 <= int(sura) <= 18):# and aya == "80":
        #     continue

        if int(sura) < SURA_MIN:
            continue
        if int(sura) > SURA_MAX:
            break

        os.makedirs("html_generated/static/%s/%s"%(sura, aya), exist_ok=True)
        words_all = data["ayat"][sura_aya]

        html = E("html", dir="rtl", lang="ar")
        head = SE(html, "head")
        meta = SE(head, "meta", {"http-equiv":"Content-Type",
                                         "content": "text/html;charset=UTF-8"})
        style = SE(head, "style")
        style.text = """
html *
{
   font-size: 130%;/* !important;*/
   color: #000 !important;
   font-family: Scheherazade, Traditional Arabic,
   Arial Unicode MS, Arial !important;
}

body {
    width: 100%;
}

table {
    empty-cells: show;
}

table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}

td {
    padding: 0.2em;
}

td.word {
    font-size: 150%;
    width: 15%;
}
        """
        body = SE(html, "body")
        div = E("div")
        aya_text = get_sura_aya_text(sura, aya)
        aya_text = re.sub(r"‍ٔ‍َ", "ـٔ‍َ", aya_text)
        aya_text = re.sub(r"ـٔ‍َ‍ٰ", r"ـٔ‍َـٰ", aya_text)
        aya_div = SE(div, "div",
            {"class": "uthmani-font read-width"}).text = aya_text
        if len(words_all) == 0:
            SE(div, "span").text = "No data for this aya in the " \
                                             "database"
        else:
            table_div = SE(div, "div", {"class": "table-words-rules-container"})
            table = SE(table_div, "table")
            tbody = SE(table, "tbody")
            tr = SE(tbody, "tr", style="background-color:#888888;")
            SE(tr, "th").text = "الكلمات"
            SE(tr, "th").text = "الروات"
            SE(tr, "th").text = "طريقة التلاوة"
        indexes_lengths = []
        for i, temp in enumerate(words_all.items()):
            words, rules = temp
            if i%2 == 0:
                clr = "#FFFFFF"
            else:
                clr = "#E0E0E0"
            attrs = {"style": "background-color:%s;"%clr}
            row = SE(table, "tr", attrs)
            SE(row, "td", {"class": "word"}, rowspan="%d"%(len(rules)+1),
               ).text = words
            # SE(row, "td")
            # SE(row, "td")
            for rule, ruwat in rules.items():
                row = SE(table, "tr", attrs)
                # SE(row, "td").text = ""
                SE(row, "td").text = ruwat
                SE(row, "td").text = rule
        rough_string = tostring(div, 'utf-8')
        reparsed = minidom.parseString(rough_string)
        s = "\n".join(reparsed.toprettyxml(indent="  ").split("\n")[1:])
        with open("html_generated/static/%s/%s/index.html"%(sura, aya),
                  "w", encoding="utf-8") as f:
            f.write(s)

if __name__ == "__main__":
    generate_html()
    # get_aya(2, 80)
