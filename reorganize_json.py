from common_imports import *


def reorganize():
    db = sqlite3.connect("qiraat.sqlite")
    db.execute("DROP TABLE IF EXISTS ayat")

    columns = [
        "sura",
        "aya",
        "words",
        "rule",
        "qari",
        "rawi",
    ]
    types = [
        "INTEGER",
        "INTEGER",
        "TEXT",
        "TEXT",
        "TEXT",
        "TEXT",
    ]
    cmd = """CREATE TABLE IF NOT EXISTS ayat(
      %s
    )"""%(",\n".join(["%s %s"%(columns[i],
                               types[i]) for i in range(len(columns))]))
    db.execute(cmd)
    db.commit()

    cmd_insert = """INSERT INTO ayat
                (%s)
                VALUES
                (%s)"""


    with open("qiraat.json", encoding="utf-8") as f:
        data = json.load(f, object_pairs_hook=OD)
    new_data = data.copy()

    new_data["ayat"] = OD()
    for aya in data["ayat"]:
        words_rules = data["ayat"][aya]
        words_all = OD()
        for words, rule, qari_rawi_list in words_rules:
            # if qari_rawi_list:
            #     try:
            #         qari, rawi = qari_rawi_list
            #     except ValueError:
            #         print(aya, qari_rawi_list)
            # else:
            #     qari = ""
            #     rawi = ""
            words = clean_up_text(words)
            rule = clean_up_text(rule)
            if words not in words_all:
                words_all[words] = OD()
            if rule not in words_all[words]:
                words_all[words][rule] = OD()
            for qari, rawi in qari_rawi_list:
                if qari not in words_all[words][rule]:
                    words_all[words][rule][qari] = []
                if rawi not in words_all[words][rule][qari]:
                    words_all[words][rule][qari].append(rawi)
                    _cmd_insert = cmd_insert%(
                        ", ".join(columns),
                        ", ".join(["?"]*len(columns)))
                    values = [int(aya.split(", ")[0]),
                         int(aya.split(", ")[1]),
                         words,
                         rule,
                         qari,
                         rawi
                         ]
                    for i, value in enumerate(values):
                        if value == "":
                            value = None
                        values[i] = value
                    db.execute(_cmd_insert, values)
                else:
                    print("Double rawi",
                        aya, words, rule, qari, rawi)
        new_data["ayat"][aya] = words_all
    db.commit()
    db.close()
    with open("qiraat.reorganized.json", "w", encoding="utf-8") as f:
        json.dump(new_data, f, indent=2, ensure_ascii=False)


if __name__ == "__main__":
    reorganize()
    # get_aya(2, 80)