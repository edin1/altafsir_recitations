import json
from collections import OrderedDict as OD

FILE_IN = 'pages_plain.json'
FILE_OUT = 'pages.json'
AYAT_PER_SURA = [ 7, 286, 200, 176, 120, 165, 206, 75, 129, 109, 123, 111, 43, 52, 99, 128, 111, 110, 98, 135, 112, 78, 118, 64, 77, 227, 93, 88, 69, 60, 34, 30, 73, 54, 45, 83, 182, 88, 75, 85, 54, 53, 89, 59, 37, 35, 38, 29, 18, 45, 60, 49, 62, 55, 78, 96, 29, 22, 24, 13, 14, 11, 11, 18, 12, 12, 30, 52, 52, 44, 28, 28, 20, 56, 40, 31, 50, 40, 46, 42, 29, 19, 36, 25, 22, 17, 19, 26, 30, 20, 15, 21, 11, 8, 8, 19, 5, 8, 8, 11, 11, 8, 3, 9, 5, 4, 7, 3, 6, 3, 5, 4, 5, 6 ];

rows = json.load(open(FILE_IN))
first_ayat_on_pages = []
page_old = 0
for row in rows:
    page, sura, aya = row
    if page != page_old:
        first_ayat_on_pages.append((sura, aya))
        page_old = page

#first_ayat_on_pages.append((sura, aya))
print(first_ayat_on_pages, len(first_ayat_on_pages))

rows_new = []
for page, sura_aya in enumerate(first_ayat_on_pages):
    page += 1
    page_next = page + 1
    print(page, page_next)
    page_ayat = []
    for sura, ayas_num in enumerate(AYAT_PER_SURA):
        sura += 1
        for aya in range(ayas_num):
            aya += 1
            if page_next < 605:
                if sura_aya <= (sura, aya) < first_ayat_on_pages[page_next-1]:
                    # print(page, sura_aya, sura, aya)
                    page_ayat.append((sura, aya))
            else: # last page
                if sura_aya <= (sura, aya):
                    page_ayat.append((sura, aya))

    rows_new.append(page_ayat)
# Last page
#rows_new.append(page_ayat)
# print(rows_new)

# The code below was needed to fill in the missing ayat
# rows_new2 = []
# for sura, sura_len in enumerate(AYAT_PER_SURA):
#     sura += 1
#     for aya in range(sura_len):
#         aya += 1
#         for page, ayat in enumerate(rows_new):
#             if min(ayat) <= (sura, aya) <= max(ayat):
#                 rows_new2.append((page+1, sura, aya))
# # print(rows_new2[-10:])
# with open(FILE_IN, 'w', encoding='utf8') as jsonfile:
#     json.dump(rows_new2, jsonfile)

with open(FILE_OUT, 'w', encoding='utf8') as jsonfile:
    json.dump(rows_new, jsonfile)
