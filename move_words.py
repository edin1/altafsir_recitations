from common_imports import *

AYA_WORDS = OD()

HURUF = ["ي", "و", "ه", "ش", "س", "ق", "ف", "غ", "ع",
           "ض", "ص", "ن", "م", "ل", "ك", "ظ", "ط", "ز", "ر", "ذ", "د",
           "خ", "ح", "ج", "ث", "ت", "ب", "ا", "ة"]

HURUF.extend(["إ", "آ", "أ", "ئ", "ء", "ؤ", "ى"])

def fix_text(text):
    res = text.replace("آ", "ا")
    res = res.replace("ء", "")
    res = res.replace("ئ", "")
    res = res.replace("أ", "ا")
    res = res.replace("إ", "ا")
    res = res.replace("ي", "ى")
    res = res.replace("لا ىستحىى ان", "لا ىستحى ان")
    res = res.replace("فاداراتم", "فادارتم")
    res = res.replace("لامىىن", "لامىن")
    res = res.replace("النبىىن", "النبىن")
    res = res.replace("القىامة", "القىمة")
    res = res.replace("وباوا", "وباو")
    res = res.replace("فباوا", "فباو")
    res = res.replace("فاوا", "فاو")
    res = res.replace("انا احىى", "انا احى")
    res = res.replace("اا", "ا")
    # res = res.replace("ىااهل", "ىا اهل")
    return res

quran_file = "quran-simple-clean.txt"
# quran_file = "quran-uthmani-min.txt"

with open(quran_file, encoding="utf-8") as f:
    _text = f.read()
_text = _text.replace("بسم الله الرحمن الرحيم ", "")
_text = _text.replace("يدعو إل", "يدعوا إل")
_text = _text.replace("والله يقبض ويبسط", "والله يقبض ويبصط")
_text = _text.replace("داوود", "داود")
_text = _text.replace("أولو الألباب", "أولوا الألباب")
_text = _text.replace("تسأموا أن", "تسئموا أن")
# _text = _text.replace("ها أنتم", "هاأنتم")
_text = _text.replace("اللذان", "الذان")
_text = _text.replace("واسألوا", "وسئلوا")
_text = _text.replace(" تبوء ", " تبوأ ")
_text = _text.replace("يا ويلتا ", "يا ويلتى ")
# _text = _text.replace("رأى", "رأا")
_text = _text.replace("أسألكم", "أسلكم")
_text = _text.replace("باسطو ", "باسطوا ")
_text = _text.replace("بسطة", "بصطة")
_text = _text.replace("وجاءوا بسحر", "وجاءو بسحر")
_text = _text.replace("سأريكم", "سأوريكم")
_text = _text.replace("واسألهم", "وسلهم")
_text = _text.replace("ستأذن", "ستئذن")
# _text = _text.replace("فاسأله", "فسله")
_text = _text.replace("فاسأل", "فسئل")
_text = _text.replace("مجراها ومرساها", "مجريها ومرساها")
_text = _text.replace("يا بني", "يابني")
_text = _text.replace("تسألن", "تسئلن")
_text = _text.replace("ويا قوم", "وياقوم")
_text = _text.replace("إن ثمود كفروا", "إن ثمودا كفروا")
_text = _text.replace("يا ويلتى", "ياويلتى")
_text = _text.replace("أولو بقية", "أولوا بقية")
_text = _text.replace("رؤيا", "رءيا")
_text = _text.replace("استيأسوا", "استيئسوا")
_text = _text.replace("واسأل القرية", "وسئل القرية")
_text = _text.replace("أشكو", "أشكوا")
_text = _text.replace(" تيأسوا", " تايئسوا")
_text = _text.replace(" ييأس", " يايئس")
_text = _text.replace("تسألهم", "تسئلهم")
_text = _text.replace("أدعو ", "أدعوا ")
_text = _text.replace("استيأس", "استيئس")
_text = _text.replace("يمحو ", "يمحوا ")
_text = _text.replace("الضعفاء للذين", "الضعفؤا للذين")
_text = _text.replace("وإنا لنحن نحيي ونميت ونحن الوارثون", "وإنا لنحن نحي ونميت ونحن الوارثون")
_text = _text.replace("علمنا المستأخرين", "علمنا المستئخرين")
_text = _text.replace("فوربك لنسألنهم أجمعين", "فوربك لنسئلنهم أجمعين")
_text = _text.replace("يتفيأ", "يتفيئوا")
_text = _text.replace("وإيتاء ذي", "وإيتائ ذي")
_text = _text.replace("أقصى", "أقصا")
_text = _text.replace("ليسوءوا", "ليسوا")
_text = _text.replace("ولا تقربوا الزنا", "ولا تقربوا الزنى")
_text = _text.replace("ونأى", "ونئا")
_text = _text.replace("بالغداة", "بالغدوة")
_text = _text.replace("لاتخذت", "لتخذت")
_text = _text.replace("19|33|والسلام", "19|33|والسلم")
_text = _text.replace("يا ابن أم", "يبنؤم")
_text = _text.replace("ومن آناء الليل", "ومن آنائ الليل")
_text = _text.replace("يا صالح", "ياصالح")
_text = _text.replace("يا ويل", "ياويل")
_text = _text.replace("تترى", "تترا")
_text = _text.replace("25|38|وعادا وثمود ", "25|38|وعادا وثمودا ")
_text = _text.replace("الليل", "اليل")
_text = _text.replace("26|61|فلما تراءى ", "26|61|فلما تراءا ")
_text = _text.replace("26|176|كذب أصحاب الأيكة", "26|176|كذب أصحاب لئيكة")
_text = _text.replace("الملأ إني", "الملؤا إني")
_text = _text.replace("27|32|قالت يا أيها الملأ", "27|32|قالت يا أيها الملؤا")
_text = _text.replace("قوة وأولو", "قوة وأولوا")
_text = _text.replace("آتاني الله", "آتان الله")
_text = _text.replace("الملأ أيكم", "الملؤا أيكم")
_text = _text.replace("تلو ", "تلوا ")
_text = _text.replace("استأجر", "استئجر")
_text = _text.replace("ملئه", "ملإيه")
_text = _text.replace("لتنوء", "لتنوأ")
_text = _text.replace("ترجو ", "ترجوا ")
_text = _text.replace("مهلكو ", "مهلكوا ")
_text = _text.replace("ثمود وقد", "ثمودا وقد")
_text = _text.replace("الناس بلقاء", "الناس بلقائ")
_text = _text.replace("أساءوا", "أسوا")
_text = _text.replace("ولقاء الآخرة فأولئك", "ولقائ الآخرة فأولئك")
_text = _text.replace("ليربو", "ليربوا")
_text = _text.replace("ناكسو", "ناكسوا")
_text = _text.replace("مستأنسين", "مستئنسين")
_text = _text.replace("فيستحيي", "فيستحي")
_text = _text.replace("تستأخرون", "تستئخرون")
_text = _text.replace("العلماء", "العلمؤا")
_text = _text.replace("نحيي ", "نحي ")
_text = _text.replace("يحيي ", "يحي ")
_text = _text.replace("لتاركو", "لتاركوا")
_text = _text.replace("لذائقو ", "لذائقوا ")
_text = _text.replace("البلاء", "البلاؤا")
_text = _text.replace("الأيكة أولئك", "لئيكة أولئك")
_text = _text.replace("حسرتا", "حسرتى")
_text = _text.replace("وجيء", "وجايء")
_text = _text.replace("كاذبا وكذلك", "كذبا وكذلك")
_text = _text.replace("فادعوا وما دعاء", "فادعوا وما دعؤا")
_text = _text.replace("شركاء شرعوا", "شركؤا شرعوا")
_text = _text.replace("وجزاء سيئة", "وجزؤا سيئة")
_text = _text.replace("أو من وراء حجاب", "أو من ورائ حجاب")
_text = _text.replace("ينشأ", "ينشؤا")
_text = _text.replace("واسأل", "وسئل")
_text = _text.replace("بلاء مبين", "بلؤا مبين")
_text = _text.replace("يدعو ", "يدعوا ")
_text = _text.replace("نبلو ", "نبلوا ")
_text = _text.replace("شطأه", "شطئه")
_text = _text.replace("بأيد ", "بأييد ")
_text = _text.replace("تمارون", "تمرون")
_text = _text.replace("وثمود فما", "وثمودا فما")
_text = _text.replace("جزاء الظالمين", "جزؤا الظالمين")
_text = _text.replace("برآء ", "برؤا ")
# _text = _text.replace("واسألوا", "وسلوا")
_text = _text.replace("تؤويه", "تئويه")
_text = _text.replace("وأن لو", "وألو")
_text = _text.replace(" سلاسل", " سلاسلا")
_text = _text.replace("قوارير من", "قواريرا من")
_text = _text.replace("إيلافهم", "إلفهم")
_text = _text.replace("رأى كوكبا", "رءا كوكبا")
_text = _text.replace("رأى القمر", "رءا القمر")
_text = _text.replace("رأى الشمس", "رءا الشمس")
_text = _text.replace("رأى أيديهم", "رءا أيديهم")
_text = _text.replace("وجاءوا أباهم", "وجاءو أباهم")
_text = _text.replace("وجاءوا على", "وجاءو على")
_text = _text.replace("رأى برهان", "رءا برهان")
_text = _text.replace("رأى قميصه", "رءا قميصه")
_text = _text.replace("رأى الذين", "رءا الذين")
_text = _text.replace("ورأى المجرمون", "ورءا المجرمون")
_text = _text.replace("رأى نارا", "رءا نارا")
_text = _text.replace("جاءوا بالإفك", "جاءو بالإفك")
_text = _text.replace("جاءوا عليه", "جاءو عليه")
_text = _text.replace("جاءوا ظلما", "جاءو ظلما")
_text = _text.replace("جاءوا قال", "جاءو قال")
_text = _text.replace("رأى المؤمنون", "رءا المؤمنون")
_text = _text.replace("جاءوا من", "جاءو من")

for _line in _text.split("\n"):
    if not _line or _line.startswith("#"):
        continue
    _sura, _aya, _aya_text = _line.split("|")
    AYA_WORDS["%s, %s"%(_sura, _aya)] = fix_text(_aya_text)

def leave_huruf_only(text):
    res = ""
    for c in text:
        if c in HURUF + [" "]:
            res += c
    return fix_text(res)

def find_sublist(list_, sublist_):
    N = len(sublist_)
    for i in range(len(list_)-N+1):
        if list_[i:i+N] == sublist_:
            return i
    return -1

def reorganize():
    with open(sys.argv[1], encoding="utf-8") as f:
        data = json.load(f, object_pairs_hook=OD)
    # Mistakes or non-suitable formating in the orginal data
    copy_words(data, "2, 286", "الْكَافِرِينَ بِسْمِ اللهِ الرَّحْمَنِ الرَّحِيمِ الم", "1, 7", "الْضَّآلِّينَ بِسْمِ اللهِ الرَّحْمَنِ الرَّحِيمِ الم")
    move_words(data, "2, 13", "2, 144", "تَرْضَاهَا")
    copy_words(data, "2, 74", "الأَنْهَارُ", "2, 266", "الأَنْهَارُ")
    move_words(data, "3, 16", "3, 30", "مُّحْضَراً وَمَا")
    move_words(data, "3, 34", "4, 34", "لِّلْغَيْبِ بِمَا")
    move_words(data, "4, 43", "6, 6", "الأَنْهَارُ")
    replace_word(data, "3, 66", "هَاأَنتُمْ", "هَا أَنتُمْ")
    replace_word(data, "3, 70", "يَاأَهْلَ", "يَا أَهْلَ")
    replace_word(data, "3, 71", "يَاأَهْلَ", "يَا أَهْلَ")
    replace_word(data, "3, 93", "يَاأَهْلَ", "يَا أَهْلَ")

    copy_words(data, "3, 93", "التَّوْرَاةُ _ بِالتَّوْرَاةِ", "3, 93", "التَّوْرَاةُ")
    copy_words(data, "3, 93", "التَّوْرَاةُ _ بِالتَّوْرَاةِ", "3, 93", "بِالتَّوْرَاةِ")
    delete_words(data, "3, 93", "التَّوْرَاةُ _ بِالتَّوْرَاةِ")

    move_words(data, "4, 1", "33, 34", "يُتْلَى")
    move_words(data, "4, 17","5, 17", "فَمَن يَمْلِكُ")
    move_words(data, "4, 29", "7, 29", "أَمَرَ رَبِّى")
    replace_word(data, "4, 54", "وَءَاتَيْنَهُم", "وَءَاتَيْنَٰهُم")
    move_words(data, "4, 80", "6, 80", "بِهِ إِلآَّ أَن")
    replace_word(data, "4, 128", "نُشُورًا أَوْ إِعْرَاضًا", "نُشُوزًا أَوْ إِعْرَاضًا")
    replace_word(data, "5, 29", "جَزَآءُ", "جَزَٰٓؤُاْ")
    move_words(data, "5, 54", "6, 54", "يُؤْمِنُونَ")
    replace_word(data, "5, 88", "طَيِّبًاوَاتَّقُواْ", "طَيِّبًا وَاتَّقُواْ")
    move_words(data, "6, 26", "8, 26", "إِذْ أَنتُمْ")
    move_words(data, "6, 26", "8, 26", "الأَرْضِ")
    replace_word(data, "8, 34", "إِن أَوْلِيَآءُهُ إِلاَّ", "إِن أَوْلِيَآؤُهُ إِلاَّ")
    replace_word(data, "9, 1", "رَسُولِهِ إِلَى", "وَرَسُولِهِ إِلَى")
    replace_word(data, "9, 57", "مُدَّخَلاً أَوْ", "مُدَّخَلاً")
    move_words(data, "9, 72", "9, 82", "كَثِيرًا")
    replace_word(data, "18, 26", "مَالَهُم", "مَا لَهُم")
    replace_word(data, "18, 28", "بِالْغَدَاوةِ", "بِالْغَدَٰوةِ")
    # replace_word(data, "19, 5", "مِن وَرَآئِ", "مِن وَرَآءِى")
    replace_word(data, "20, 80", "يَا بَنِى إِسْرَآءِيلَ", "يَابَنِى إِسْرَآءِيلَ")
    replace_word(data, "20, 130", "وَمِنْ ءَانَآىءِ", "وَمِنْ ءَانَآئِ")
    replace_word(data, "21, 16", "الأَرْضَ", "وَالْأَرْضَ")
    replace_word(data, "21, 23", "هُمْ", "وَهُمْ")
    # A bit difficult, words are mixed up, need to delete some rules, and "borrow" a word from another aya
    replace_word(data, "23, 35", "وَعِظَاماً أَنَّكُمْ إِذًا", "وَعِظَاماً أَنَّكُمْ")
    _tmp_rules = [
        "بصلة الميم ومدها حركتين أو ثلاث، وإسكان الميم دون سكت عليها",
        "بصلة الميم ومدها 6 حركات",
        "بصلة الميم ومدها حركتين",
        "بإسكان الميم دون سكت عليها",
        "بإسكان الميم مع السكت عليها، وبإسكان الميم دون السكت عليهت",
    ]
    for rule in _tmp_rules:
        delete_rule(data, "23, 35", "وَعِظَاماً أَنَّكُم", rule)
    # We also need to "borrow" a word from another aya
    copy_words(data, "23, 33", "وَأَتْرَفْنَاهُمْ", "23, 35", "أَنَّكُم مُّخْرَجُونَ")

    move_words(data, "24, 4", "24, 6", "شُهَدَآءُ إِلآَّ أَنفُسُهُمْ")
    move_words(data, "25, 5", "25, 25", "السَّمَآءُ")
    replace_word(data, "27, 20", "مَالِىَ", "مَا لِيَ")
    move_words(data, "28, 31", "29, 31", "إِبْرَاهِيمَ")
    replace_word(data, "30, 8", "بِلِقَآئِى", "بِلِقَآئِ")
    replace_word(data, "30, 10", "أَسَآؤُاْ", "أَسَٰٓـٔ‍ُواْ")
    replace_word(data, "30, 15", "رَوْضَةٍ يُجْبَرُونَ", "رَوْضَةٍ يُحْبَرُونَ")
    replace_word(data, "30, 16", "وَلِقَآىءِ", "وَلِقَآئِ")
    replace_word(data, "33, 46", "وَمُبَشِّراً وَدَاعِياً", "وَنَذِيراً وَدَاعِياً")
    replace_word(data, "34, 24", "الأَرْضِ", "وَالأَرْضِ")
    delete_words(data, "35, 36", "كُفْرُهُمْ إِلاَّ")
    replace_word(data, "35, 40", "بَيِّنَاتٍ", "بَيِّنَتٍ")
    replace_word(data, "37, 2", "فَالزَّاجِرَاتِ رَجْرًا", "فَالزَّاجِرَاتِ زَجْرًا")
    replace_word(data, "41, 44", "ءَاْعْجَمِىٌّوَعَرَبِىٌّ", "ءَاْعْجَمِىٌّ وَعَرَبِىٌّ")
    move_words(data, "42, 11", "42, 20", "الدُّنْيَا")
    replace_word(data, "42, 51", "مِن وَرَآءِى", "مِن وَرَآئِ")
    replace_word(data, "42, 52", "وَأَوْحَيْنَآ إِلَيْكَ", "أَوْحَيْنَآ إِلَيْكَ")
    move_words(data, "43, 5", "43, 3", "قُرْءَانًا")
    move_words(data, "43, 12", "43, 20", "عَبَدْنَاهُم")
    replace_word(data, "45, 27", "الأَرْضِ", "وَالأَرْضِ")
    delete_words(data, "47, 38", "تَبَيَّنَ لَهُمُ")
    move_words(data, "51, 52", "53, 52", "إِنَّهُمْ")
    move_words(data, "56, 27", "56, 25", "تَأْثِيمًا إِلاَّ")

    for sura_aya in data["ayat"]:
        if data["ayat"]["1, 1"] != OD():
            print(sura_aya)
            data["ayat"]["1, 1"]
            raise Exception("1, 1 has changed")
        sura, aya = sura_aya.split(", ")
        # if int(sura) < 18:
        #     continue
        # if int(aya) > 199:
        #     break
        if int(sura) < SURA_MIN:
            continue
        if int(sura) > SURA_MAX:
            break
        # if sura_aya != "18, 85":
        #     continue
        words_all = data["ayat"][sura_aya]
        indexes_lengths = []
        _items = list(words_all.items())
        for words_orig, rules in _items:
            words = clean_up_text(words_orig)
            words_list = words.split()
            words_huruf_only = leave_huruf_only(words).split()
            text_current = AYA_WORDS[sura_aya].split()
            index = find_sublist(text_current, words_huruf_only)
            if index != -1:
                indexes_lengths.append((index, len(words), words, rules))
                continue
            # More than one word
            else:
                # Try to find the word in the current and next aya
                if sura_aya != "114, 6":
                    if int(aya)+1 <= AYAT_PER_SURA[int(sura)-1]:
                        sura_aya_next = "%d, %d"%(int(sura), int(aya)+1)
                        text_next = text_current + AYA_WORDS[sura_aya_next].split()
                    else: # last aya
                        if int(sura) != 8:
                            # Just copy the basmala from the fatiha
                            text_next = text_current + AYA_WORDS["1, 1"].split()
                            # and add to that the first aya of the next sura
                            sura_aya_next = "%d, 1"%(int(sura)+1)
                            text_next += AYA_WORDS[sura_aya_next].split()
                        else: # For sura tawba (No. 9, after sura 8)
                            sura_aya_next = "%d, 1"%(int(sura)+1)
                            text_next = text_current + AYA_WORDS[sura_aya_next].split()
                    # print(sura_aya_next)
                    # print(text_next, words_huruf_only)
                    index = find_sublist(text_next, words_huruf_only)
                else:
                    raise Exception("Final aya in the Qur'an, and couldn't find"
                                    " the word")
                if index != -1:
                    if index > len(text_current):
                        print(index, len(text_current))
                        print(sura_aya, words)
                        print(text_current)
                        print("The word(s) is/are in the next aya")
                        move_words(data, sura_aya, sura_aya_next, words_orig)
                        continue
                        # raise Exception()
                    indexes_lengths.append((index, len(words), words, rules))
                    continue
                # Now try to find the words in the previous aya
                # We instantiate the variable here because there are some special cases
                elif sura_aya != "1, 1":
                    text_prev = []
                    if int(aya) != 1:
                        sura_aya_prev = "%d, %d"%(int(sura), int(aya)-1)
                    else:
                        sura_aya_prev = "%d, %d"%(int(sura)-1, AYAT_PER_SURA[int(sura)-2])
                        if int(sura) != 9:
                            # First we add the basmala from the fatiha to the current aya
                            text_current = AYA_WORDS["1, 1"].split() + text_current
                        else: # For sura tawba we will add the last aya from sura 8, without basmala
                            pass
                    # print(sura_aya_prev)
                    text_prev += AYA_WORDS[sura_aya_prev].split() + text_current
                    # print(text_prev)
                    index = find_sublist(text_prev, words_huruf_only)
                    if index != -1:
                        print("Moving words to previous aya. "
                              "Please rerun the script when it finishes "
                              "in order to incorporate the new changes")
                        move_words(data, sura_aya, sura_aya_prev, words_orig)
                        continue
                    else:
                        print(sura_aya)
                        print(words_orig)
                        print(words_huruf_only)
                        print(text_current)
                        raise Exception("Cannot find word in aya")
                else:
                    # After last sura, if one begins reciting the Qur'an from
                    # the beginning, there is no special rule involved
                    print(sura_aya)
                    print(words_orig)
                    print(words_huruf_only)
                    raise Exception("This bug shouldn't happen")
        indexes_lengths.sort()
        data["ayat"][sura_aya] = OD()
        for i in indexes_lengths:
            data["ayat"][sura_aya][i[2]] = i[3]
    # print(data["ayat"]["8, 75"])
    data["ayat"]["8, 75"]["عَلِيمٌ بَرَآءَةٌ"]["دون بسملة لجميع القراء ويجوز الوصل والسكت والقطع بين عَلِيمٌ و بَرَآءَةٌ."] = OD()
    for qari in QURRA_RUWAT:
        data["ayat"]["8, 75"]["عَلِيمٌ بَرَآءَةٌ"]["دون بسملة لجميع القراء ويجوز الوصل والسكت والقطع بين عَلِيمٌ و بَرَآءَةٌ."][qari] = ["متفق عليه"]
    with open("qiraat.moved_words.json", "w", encoding="utf-8") as f:
        print("Saving changes...")
        json.dump(data, f, indent=2, ensure_ascii=False)


if __name__ == "__main__":
    reorganize()
    # get_aya(2, 80)
