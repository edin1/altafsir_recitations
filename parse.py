from common_imports import *

DATASTRUCTURE = OD()

QURRA = set()
RUWAT = set()

def get_qari_rawi_container(tag):
    # pprint(dir(tag))
    res = tag
    chain = []
    while True:
        chain.append(res)
        res = res.findChild()
        if res is None:
            break
    # pprint(chain)
    return chain[-3]

def get_innermost_text(tag):
    parent = tag
    while True:
        child = parent.findChild()
        if child is None:
            return clean_up_text(parent.text)
        parent = child

def get_page_table(sura, aya, page):
    fname = "html_downloaded/%03d/%03d/index%d.html"%(sura, aya, page)
    if not os.path.exists(fname):
        return None
    with open(fname, encoding="utf-8", newline="\n") as f:
        text = f.read()
        soup = BeautifulSoup(text)
        div = soup.find(id="SearchResults")
        return div.table

def get_aya(sura, aya):
    tables = [] # Just something different from None
    page = 0
    while True:
        page += 1
        table = get_page_table(sura, aya, page)
        if table is None:
            break
        print(page)
        tables.append(table)
    text = ""
    words_rules = []
    for table in tables:
        for tr in table.children:
            if tr.name != "tr":
                print("Element not tr", tr)
                print(type(tr))
                continue
            try:
                _cls = tr['class']
            except KeyError:
                continue
            if _cls == ["SearchResultBody"]:
                children = list(tr.children)
                words = clean_up_text(children[0].a.text)
                qari_rawi_list = []
                tag = get_qari_rawi_container(children[1].table)
                for tr in tag:
                    # print(tr, type(tr))
                    qari = clean_up_text(tr.find_all("td")[0].text)
                    QURRA.add(qari)
                    rawi = clean_up_text(tr.find_all("td")[1].text)
                    RUWAT.add(rawi)
                    qari_rawi_list.append((qari, rawi))
                rule = get_innermost_text(children[2])
                words_rules.append([words, rule, qari_rawi_list])
                # print(words)
                # INDENT = "    "
                # indent = INDENT
                # print(indent + rule)
                # indent += INDENT
                # for qari, rawi in qari_rawi_list:
                #     print("%s%s, %s"%(indent, qari, rawi))
    return words_rules
    # with open("index.html", "w", encoding="utf-8", newline="\n") as f:
    #     f.write('<table dir="rtl">' + text + "</table>")
    # print(r.encoding)

def parse_html():
    DATASTRUCTURE["ayat"] = OD()
    for sura, ayat_number in enumerate(ayat_per_sura):#([7]):
        sura += 1
        # DATASTRUCTURE["suras"][sura] = OD()
        for aya in range(ayat_number):
            aya += 1
            print("Parsing local html for aya %s:%s"%(sura, aya))
            words_rules = get_aya(sura, aya)
            DATASTRUCTURE["ayat"]["%d, %d"%(sura, aya)] = words_rules
    DATASTRUCTURE["qurra"] = list(QURRA)
    DATASTRUCTURE["ruwat"] = list(RUWAT)
    with open("qiraat.json", "w", encoding="utf-8") as f:
        json.dump(DATASTRUCTURE, f, indent=2, ensure_ascii=False)

if __name__ == "__main__":
    parse_html()
    # get_aya(2, 80)