from common_imports import *

def download_page_table(sura, aya, page):
    data = {
            'SoraName': '%d'%sura,
            'Ayat': '%d'%aya,
            'rDisplay': "yes",
            'PageNum': "%d"%page,
            'PageSize': "50",
            }
    enc = "cp1256"
    r = requests.post("http://altafsir.com/Recitations.asp?img=A", data=data)
    soup = BeautifulSoup(r.text)
    div = soup.find(id="SearchResults")
    if div.table is not None:
        with open("html/%03d/%03d/index%d.html"%(sura, aya, page), "w", encoding="utf-8", newline="\n") as f:
            f.write(r.text)
        return div.table

def download_aya(sura, aya):
    page = 0
    while True:
        page += 1
        table = download_page_table(sura, aya, page)
        if table is None:
            break
        print(page)

def download_html():
    try:
        dirs = [d for d in os.listdir("html")
                if os.path.isdir("html/%s"%d)
                and d in ["%03d"%(s+1) for s in range(114)]]
        last_sura = max(dirs)
    except (FileNotFoundError, ValueError):
        last_sura = "001"

    try:
        dirs = [d for d in os.listdir("html/%s"%last_sura)
                if os.path.isdir("html/%s/%s"%(last_sura, d)) and d in
                ["%03d"%(a+1) for a in range(ayat_per_sura[int(last_sura)-1])]]
        last_aya = max(dirs)
    except (FileNotFoundError, ValueError):
        last_aya = "001"
    for sura, ayat_number in enumerate(ayat_per_sura):
        sura += 1
        sura = "%03d"%sura
        if os.path.exists("html/%s"%sura)and sura < last_sura:
            continue
        os.makedirs("html/%s"%sura, exist_ok=True)
        for aya in range(ayat_number):
            aya += 1
            aya = "%03d"%aya
            if os.path.exists("html/%s/%s"%(sura, aya))\
                    and aya < last_aya:
                continue
            os.makedirs("html/%s/%s"%(sura, aya), exist_ok=True)
            print("Downloading html for aya %s:%s"%(sura, aya))
            download_aya(int(sura), int(aya))

if __name__ == "__main__":
    pass
