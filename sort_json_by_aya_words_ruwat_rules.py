from common_imports import *

def reorganize():
    with open(sys.argv[1], encoding="utf-8") as f:
        data = json.load(f, object_pairs_hook=OD)
        new_data = data.copy()

        new_data["ayat"] = OD()
        for aya in data["ayat"]:
            print(aya)
            words_all = data["ayat"][aya]
            new_data["ayat"][aya] = OD()
            for words in words_all:
                rules = words_all[words]
                ruwat_qurra_rule_dict = OD()
                for rule in rules:
                    if not rule:
                        continue
                    qurra = rules[rule]
                    for qari in qurra:
                        if not qari:
                            for key in RUWAT_QURRA_LIST:
                                ruwat_qurra_rule_dict[key] = rule
                            break
                        for rawi in qurra[qari]:
                            if rawi == "متفق عليه":
                                for _rawi in QURRA_RUWAT[qari]:
                                    key = (_rawi, qari)
                                    add_key_value_list(
                                        ruwat_qurra_rule_dict, key, rule)
                            else:
                                key = (rawi, qari)
                                add_key_value_list(
                                    ruwat_qurra_rule_dict, key, rule)
                rule_ruwat_qurra_dict = OD()
                # pprint(list(ruwat_qurra_rule_dict.keys()))
                # print(words)
                if ruwat_qurra_rule_dict:
                    for key in RUWAT_QURRA_LIST:
                        # TODO: check missing keys/rules/qaris/rawis
                        try:
                            rules = ruwat_qurra_rule_dict[key]
                        except KeyError:
                            rules = [""]
                            # continue
                        for rule in rules:
                            add_key_value_list(
                                rule_ruwat_qurra_dict, rule, key)
                # Simplify the ruwat list for rules
                rule_ruwat_simple_dict = OD()
                for rule, rawi_qari_list in rule_ruwat_qurra_dict.items():
                    ruwat = []
                    for rawi, qari in rawi_qari_list:
                        if rawi == "الدوري":
                            if qari == "أبو عمرو بن العلاء":
                                ruwat.append("الدوري عن أبي عمرو")
                            elif qari == "الكسائي الكوفي":
                                ruwat.append("الدوري عن الكسائي")
                            else:
                                raise Exception("Unrecognized qari for Duri")
                        else:
                            ruwat.append(rawi)
                    rule_ruwat_simple_dict[rule] = "، ".join(ruwat)
                new_data["ayat"][aya][words] = rule_ruwat_simple_dict
                # new_data["ayat"][aya][words] = rule_ruwat_qurra_dict
    with open("qiraat.%s.json"%os.path.splitext(os.path.basename(__file__))[0],
              "w", encoding="utf-8") as f:
        json.dump(new_data, f, indent=2, ensure_ascii=False)


if __name__ == "__main__":
    reorganize()
    # get_aya(2, 80)