import re
fname = "common_imports.py"
with open(fname) as f:
    text = f.read()

text = re.sub("SURA_MIN = (\d+)", lambda m: "SURA_MIN = %d"%(int(m.groups()[0])+5), text)

with open(fname, "w") as f:
    f.write(text)
